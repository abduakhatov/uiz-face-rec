from train.inception import *
import dataset
import argparse

def main(data_dir, epochs, model_path):
    epochs = 1  # 40
    model_name = 'facenet_weights'
    model_file_name = f"{model_name}_RMSp_MSE_RMSE.h5"
    checkpoint_path = os.path.join(model_path, model_name, model_file_name)

    try:
        os.mkdir(os.path.join(model_path, model_name))
    except:
        print(f">>> Folder {model_name} exists")
    try:
        os.mkdir(os.path.join(model_path, 'post_trained'))
    except:
        print(f">>> Folder post_trained exists")
    try:
        os.mkdir(os.path.join(model_path, 'pre_trained'))
    except:
        print(f">>> Folder pre_trained exists")

    model = loadModel()
    fit(model,
        *dataset.load_dataset(data_dir=data_dir),
        epochs=epochs,
        checkpoint=checkpoint_path,
        trained_model=os.path.join(model_path, model_file_name)
        )

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Main training")
    parser.add_argument('--data_dir', help="data dir", default='/home/sa/dataset/t')
    parser.add_argument('--epochs', type=int, default=30)
    parser.add_argument('--model_path', default = 'models/post_trained')
    args = parser.parse_args()
    main(epochs=args.epochs, data_dir=args.data_dir, model_path=args.model_path)
