from tensorflow.keras.preprocessing import image_dataset_from_directory
from tensorflow.keras.preprocessing.image import ImageDataGenerator


def load_dataset(data_dir,
                 batch_size=32,
                 img_height=160,
                 img_width=160):
    # train_datagen = ImageDataGenerator(
    #     rescale=1./255,
    #     validation_split=0.2)  # set validation split
    # train_gen = train_datagen.flow_from_directory(
    #     data_dir,
    #     target_size=(img_height, img_width),
    #     batch_size=batch_size,
    #     seed=123,
    #     subset='training')  # set as training data
    # val_gen = train_datagen.flow_from_directory(
    #     data_dir,  # same directory as training data
    #     target_size=(img_height, img_width),
    #     batch_size=batch_size,
    #     seed=123,
    #     subset='validation')  # set as validation data
    # test_datagen = ImageDataGenerator(rescale=1./255)
    train_gen = image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="training",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)
    val_gen = image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="validation",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)
    return train_gen, val_gen
